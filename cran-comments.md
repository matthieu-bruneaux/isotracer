## Reason for submitting an updated version of the package (1.1.6 -> 1.1.7)

- I was made aware of a test failure for our package by messages from CRAN and
  from Prof. Ripley on 2024-10-22. This updated version is submitted to avoid
  triggering this test failure in the future.

## Summary of update

Main updates:

- We set less stringent thresholds when comparing numerical output of model
  calculations in the test suite (changing from 1e-13 to 1e-12 as the maximum
  allowed numerical discrepancy).
- Package version was bumped to 1.1.7.

Justification of the update:

- The test failure was triggered on rare occasions while checking the numerical
  consistency of a model output. The threshold we used to take into account
  limited precision in numerical calculations turned out to be too strict, and
  the test failure was only rarely triggered due to the stochastic nature of
  the test object (output from an MCMC run).

## Test environments (on 2024-11-04)

- Debian 6.1.112-1, R 4.2.2 Patched (2022-11-10 r83330) -- run locally
- Windows Server 2022 x64 (build 20348), R 4.4.1 (2024-06-14 ucrt) -- run on
  https://win-builder.r-project.org/
- Windows Server 2022 x64 (build 20348), R Under development (unstable)
  (2024-11-03 r87286 ucrt) -- run on https://win-builder.r-project.org/

## Summary of R CMD check results

There were no ERRORs or WARNINGs.

There were 3 NOTES:


*  checking installed package size ... NOTE
  installed size is  8.6Mb
  sub-directories of 1Mb or more:
    data   2.0Mb
    doc    2.3Mb
    libs   3.2Mb 

  The size of the locally built tarball is 4030962 bytes, respecting the 5 MB
  limit for CRAN packages.


*  checking dependencies in R code ... NOTE
Namespace in Imports field not imported from: ‘rstantools’
  All declared Imports should be used.

  'rstantools' is present in the Imports field so that the package
  installation/configuration is delegated to rstantools for compatibility with
  future releases of rstan/StanHeaders. While rstantools is not used by our
  package code in the './R/' folder, it is used in the files './configure' and
  './configure.win'.


*  checking for GNU extensions in Makefiles ... NOTE
GNU make is a SystemRequirements. 

  The GNU make requirement is introduced by the dependency on the rstan
  package.


## Downstream dependencies

There are currently no downstream dependencies for this package.
